<?php
require_once 'lib/Model.php';

class ProductModel extends Model{
    
    const PAGE_SIZE = 3; 
    
    function __construct()
    {
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM producto WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {      
    }

    public function getAll()
    {
        $this->_sql = "SELECT * FROM producto ORDER BY id";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO producto (codigo, nombre, precio, existencia)"
                . " VALUES ('$fila[codigo]', '$fila[nombre]', '$fila[precio]', '$fila[existencia]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE producto SET "
                . " codigo='$row[codigo]', "
                . " nombre='$row[nombre]',"
                . " precio=$row[precio],"
                . " existencia='$row[existencia]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();
    }
    
    public function ajaxPageData($pageNumber)
    {
        $this->_sql = "SELECT CEILING (COUNT(id)/" . $this::PAGE_SIZE . ") AS pages  FROM producto" ;
        $this->executeSelect();
        $page[] = $pageNumber;
        $page[] = $this->_rows[0]['pages'];
        
        $this->_sql ="SELECT producto.*  FROM producto ORDER BY codigo LIMIT " . $this::PAGE_SIZE * ($pageNumber-1) . ','. $this::PAGE_SIZE;
        $this->executeSelect();
        $page[] = $this->_rows;
        return $page;
    }
}