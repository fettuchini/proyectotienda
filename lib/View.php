<?php
require_once 'smarty/libs/Smarty.class.php';
require_once 'lib/Lang.php';

class View{
    public $smarty;
    private $_method;
    public $lang;
            
            
    
    function getMethod()
    {
        return $this->_method;
    }

    function setMethod($method)
    {
        $this->_method = $method;
    }

    
    function __construct()
    {
        $this->lang = new Lang();
        
        //preparar smarty
        $this->smarty = new Smarty();
        $this->smarty->template_dir = 'template';
        $this->smarty->compile_dir = 'tmp';
        
        //asignar variables para las cabeceras/pies
        $this->smarty->assign('lang', $_SESSION['lang']);
       if(isset($_SESSION['name'])){
            $this->smarty->assign('name', $_SESSION['name']); 
            
        }else{
            $this->smarty->assign('name', "");
            
        }
        
        
        
//         var_dump($_SESSION);   
        
       if($_SESSION['accessLevel'] == 1){
              $this->smarty->assign('href', '/login');   
               $this->smarty->assign('login', 'login');   
        }else{
              $this->smarty->assign('href', '/login/unlogin');   
              $this->smarty->assign('login', 'unlogin');                          
        }
        
        $this->smarty->assign('language', $this->lang);
        $this->smarty->assign('url', Config::URL);

    }    
    
}