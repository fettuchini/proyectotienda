<?php
require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Product extends Controller{
    private $_levelModel; //= new LevelModel();

    function __construct()
    {
        parent::__construct('Product');
        $this->_levelModel = new LevelModel;
    }

   
    public function index()
    {
        //mostrar lista de todos los registros.
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
    public function ajaxPageData($pageNumber)
    {
       $page = $this->model->ajaxPageData($pageNumber);
       echo json_encode($page);
    }
    
     
    public function insert()
    {
        $row = $_POST;  
        $this->model->insert($row);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
    }
    public function delete($id)
    {
        $this->model->delete($id);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
    }
    
    public function update()
    {
        $row = $_POST; 
        $error = $this->_validate($row);
        if (count($error)){
            $this->edit($row['id'], $error);
        }
        else{
            $row['password'] = md5($row['password']);
            $this->model->update($row);    
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
        }
    }

}