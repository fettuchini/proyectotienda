<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author usuario
 */
require_once 'lib/Controller.php';



class Login extends Controller{
   

    function __construct()
    {
        parent::__construct('Login');
        
    }
    
    function index(){
        
        $this->view->render();
        
    }
    
    function access(){
        
        $row = $_POST;  
        $row['password'] = md5($row['password']);
        
        $loginRow=$this->model->check($row);   
               
       if(isset($loginRow[0]['idRole'])){
          
        $_SESSION['accessLevel'] = $loginRow[0]['idRole'];
        $_SESSION['name'] = $loginRow[0]['nombre'];
        //$_COOKIE['name'] = $_SESSION['name'];
       
       header('Location: ' . Config::URL . $_SESSION['lang'] . '/index');
       
        
       }else{
           
       header('Location: ' . Config::URL . $_SESSION['lang'] . '/login');
//       var_dump($_SESSION);
      
       }
    }
    
    
    function unlogin(){
        unset($_SESSION);
        session_destroy();
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/index');
        
//        var_dump($_SESSION);
        
    }
    
    
    
}
