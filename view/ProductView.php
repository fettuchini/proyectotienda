<?php

require_once 'lib/View.php';

class ProductView extends View
{
    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($rows, $template='product.tpl')
    {
        $js[] = 'ajaxProduct.js';
        $this->smarty->assign('js', $js);
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }
    
   /* public function edit($row, $error="", $levelRows)
    {
        $template='studyFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('levelRows', $levelRows);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }   */
}
